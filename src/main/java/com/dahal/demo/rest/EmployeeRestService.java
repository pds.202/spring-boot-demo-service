package com.dahal.demo.rest;

import com.dahal.demo.Entity.Employee;
import com.dahal.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/emp")
public class EmployeeRestService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @RequestMapping(value = "/{id}", method= RequestMethod.GET)
    public Optional<Employee> getEmployee(@PathVariable("id") Integer id){
        return employeeRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Employee> getEmployees(){
        return employeeRepository.findAll();
    }

    @RequestMapping(method= RequestMethod.POST)
    public Employee postEmployee(@RequestBody Employee e){
        return employeeRepository.save(e);
    }

    @RequestMapping(value = "/{id}", method= RequestMethod.DELETE)
    public void deleteEmployee(@PathVariable("id") Integer id){
        employeeRepository.deleteById(id);
    }

    @PostConstruct
    public void initializeData(){
        Employee e1 = new Employee(1,"John Roberts", "77 W. Madison Ave");
        Employee e2 = new Employee(2,"Suraj Humagain", "2250 South Street");
        Employee e3 = new Employee(3,"Binita Thapa", "1 Infinite Loop");
        employeeRepository.saveAll(Arrays.asList(e1,e2,e3));
    }

}
