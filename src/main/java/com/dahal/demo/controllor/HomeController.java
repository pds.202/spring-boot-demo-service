package com.dahal.demo.controllor;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@Controller
public class HomeController {

    @RequestMapping(value="/", method = RequestMethod.GET)
    public String main(Map<String,Object> model){
        model.put("message","Welcome to Homepage");
        return "index";
    }

    @RequestMapping(value="/next", method = RequestMethod.GET)
    public String next(Map<String,Object> model){
        model.put("message","Welcome to NextPage");
        return "next";
    }
}
